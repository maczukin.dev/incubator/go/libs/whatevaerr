package whatevaerr

type e struct{}

func (e *e) Error() string {
	return "whateva..."
}

func (e *e) Is(_ error) bool {
	return true
}

var Err = new(e)
