# WhatevaErr

Because why not? :D

You want to test your go app for errors, but you don't really care about them?

Well, whateva..

## How to use

```golang
package test

import "go.maczukin.dev/whatevaerr"

func test() {
	err := call()
	if errors.Is(whatevaerr.Err, err) {
        // do something
    }
}
```

## License

- [MIT](LICENSE) Copyright (c) 2024-now Tomasz Maczukin
