package whatevaerr_test

import (
	"errors"
	"testing"

	"go.maczukin.dev/whatevaerr"
)

func TestEError(t *testing.T) {
	if whatevaerr.Err.Error() != "whateva..." {
		t.Error("unexpected output")
	}
}

func TestEIs(t *testing.T) {
	err1 := errors.New("e1")
	err2 := errors.New("e2")

	if !errors.Is(whatevaerr.Err, err1) {
		t.Error("Expected errors to match")
	}

	if !errors.Is(whatevaerr.Err, err2) {
		t.Error("Expected errors to match")
	}
}
